<?php

namespace Expolaroid\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="Expolaroid\AdminBundle\Entity\EventExpolaroidRepository")
 */
class EventExpolaroid
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var integer
     *
     * @ORM\Column(name="id_user", type="string")
     */
    protected $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     *
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     *
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     *
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="town", type="string", length=255)
     *
     */
    private $town;

    /**
     * @var string
     *
     * @ORM\Column(name="start_day", type="string", length=255)
     *
     */
    private $startDay;

    /**
     * @var string
     *
     * @ORM\Column(name="start_month", type="string", length=255)
     *
     */
    private $startMonth;

    /**
     * @var string
     *
     * @ORM\Column(name="end_day", type="string", length=255)
     *
     */
    private $endDay;

    /**
     * @var string
     *
     * @ORM\Column(name="end_month", type="string", length=255)
     *
     */
    private $endMonth;

    /**
     * @var string
     *
     * @ORM\Column(name="vernissage_day", type="string", length=255, nullable=true)
     *
     */
    private $vernissageDay;

    /**
     * @var string
     *
     * @ORM\Column(name="vernissage_month", type="string", length=255, nullable=true)
     *
     */
    private $vernissageMonth;

    /**
     * @var string
     *
     * @ORM\Column(name="no_vernissage", type="string", length=255, nullable=true)
     *
     */
    private $noVernissage;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true, nullable=true)
     *
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="participants", type="string", length=255)
     *
     */
    private $participants;

    /**
     * @var string
     *
     * @ORM\Column(name="siteWeb", type="string", length=255, nullable=true)
     *
     */
    private $siteWeb;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable=true)
     *
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="validation_staff", type="string", length=255)
     *
     */
    private $validation_staff;

    /**
     * @var string
     *
     * @ORM\Column(name="motif", type="string", length=255, nullable=true)
     *
     */
    private $motif;

    /**
     * @var string
     *
     * @ORM\Column(name="urlPhoto", type="string", length=255, nullable=true)
     *
     */
    private $urlPhoto;

    /**
     * @var string
     *
     * @ORM\Column(name="urlMini", type="string", length=255, nullable=true)
     *
     */
    private $urlMini;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Event
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Event
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set idUser
     *
     * @param string $idUser
     *
     * @return Event
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return string
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set startDay
     *
     * @param string $startDay
     *
     * @return Event
     */
    public function setStartDay($startDay)
    {
        $this->startDay = $startDay;

        return $this;
    }

    /**
     * Get startDay
     *
     * @return string
     */
    public function getStartDay()
    {
        return $this->startDay;
    }

    /**
     * Set startMonth
     *
     * @param string $startMonth
     *
     * @return Event
     */
    public function setStartMonth($startMonth)
    {
        $this->startMonth = $startMonth;

        return $this;
    }

    /**
     * Get startMonth
     *
     * @return string
     */
    public function getStartMonth()
    {
        return $this->startMonth;
    }

    /**
     * Set endDay
     *
     * @param string $endDay
     *
     * @return Event
     */
    public function setEndDay($endDay)
    {
        $this->endDay = $endDay;

        return $this;
    }

    /**
     * Get endDay
     *
     * @return string
     */
    public function getEndDay()
    {
        return $this->endDay;
    }

    /**
     * Set $endMonth
     *
     * @param string $endMonth
     *
     * @return Event
     */
    public function setEndMonth($endMonth)
    {
        $this->endMonth = $endMonth;

        return $this;
    }

    /**
     * Get endMonth
     *
     * @return string
     */
    public function getEndMonth()
    {
        return $this->endMonth;
    }

    /**
     * Set vernissageDay
     *
     * @param string $vernissageDay
     *
     * @return Event
     */
    public function setVernissageDay($vernissageDay)
    {
        $this->vernissageDay = $vernissageDay;

        return $this;
    }

    /**
     * Get vernissageDay
     *
     * @return string
     */
    public function getVernissageDay()
    {
        return $this->vernissageDay;
    }

    /**
     * Set vernissageMonth
     *
     * @param string $vernissageMonth
     *
     * @return Event
     */
    public function setVernissageMonth($vernissageMonth)
    {
        $this->vernissageMonth = $vernissageMonth;

        return $this;
    }

    /**
     * Get vernissageMonth
     *
     * @return string
     */
    public function getVernissageMonth()
    {
        return $this->vernissageMonth;
    }


    /**
     * Set noVernissage
     *
     * @param string $noVernissage
     *
     * @return Event
     */
    public function setNoVernissage($noVernissage)
    {
        $this->noVernissage = $noVernissage;

        return $this;
    }

    /**
     * Get noVernissage
     *
     * @return string
     */
    public function getNoVernissage()
    {
        return $this->noVernissage;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set participants
     *
     * @param string $participants
     *
     * @return Event
     */
    public function setParticipants($participants)
    {
        $this->participants = $participants;

        return $this;
    }

    /**
     * Get participants
     *
     * @return string
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * Set siteWeb
     *
     * @param string $siteWeb
     *
     * @return Event
     */
    public function setSiteWeb($siteWeb)
    {
        $this->siteWeb = $siteWeb;

        return $this;
    }

    /**
     * Get siteWeb
     *
     * @return string
     */
    public function getSiteWeb()
    {
        return $this->siteWeb;
    }

    /**
     * Set contact
     *
     * @param string $contact
     *
     * @return Event
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set validation_staff
     *
     * @param string $validation_staff
     *
     * @return Event
     */
    public function setValidation_staff($validation_staff)
    {
        $this->validation_staff = $validation_staff;

        return $this;
    }

    /**
     * Get validation_staff
     *
     * @return string
     */
    public function getValidation_staff()
    {
        return $this->validation_staff;
    }

    /**
     * Set motif
     *
     * @param string $motif
     *
     * @return Event
     */
    public function setMotif($motif)
    {
        $this->motif = $motif;

        return $this;
    }

    /**
     * Get motif
     *
     * @return string
     */
    public function getMotif()
    {
        return $this->motif;
    }

    /**
     * Set urlPhoto
     *
     * @param string $urlPhoto
     *
     * @return Event
     */
    public function setUrlPhoto($urlPhoto)
    {
        $this->urlPhoto = $urlPhoto;

        return $this;
    }

    /**
     * Get urlPhoto
     *
     * @return string
     */
    public function getUrlPhoto()
    {
        return $this->urlPhoto;
    }

    /**
     * Set urlMini
     *
     * @param string $urlMini
     *
     * @return Event
     */
    public function setUrlMini($urlMini)
    {
        $this->urlMini = $urlMini;

        return $this;
    }

    /**
     * Get urlMini
     *
     * @return string
     */
    public function getUrlMini()
    {
        return $this->urlMini;
    }

    /**
     * Set town
     *
     * @param string $town
     *
     * @return Event
     */
    public function setTown($town)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

}

