<?php

namespace Expolaroid\AdminBundle\Controller;

use Expolaroid\AdminBundle\Entity\EventExpolaroid;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

    public function autorized()
    {
        $autorized = array('guillaume@teillet.eu', 'alin_nantes@yahoo.fr', 'roblin.mathieu@gmail.com');
        $user = $this->container->get('security.context')->getToken()->getUser();
        $i = 0;
        $ok = 0;
        while(isset($autorized[$i]))
        {
            if ($user->getEmail() == $autorized[$i])
                $ok =1;
            $i++;
        }

        return $ok;
    }

    public function compteARebourg()
    {
        $expolaroid = mktime(0, 0, 0, 4, 1, 2016);
        $expolaroid_fin = mktime(0, 0, 0, 5, 1, 2016);
        $label = "DÉBUT D'EXPOLAROID 2016";

         if ($expolaroid < time() && $expolaroid_fin > time()) // time())
         {
             $expolaroid = mktime(0, 0, 0, 5, 1, 2016);
             $label = "FIN D'EXPOLAROID 2016";
         }
        elseif (time() > $expolaroid && time() > $expolaroid_fin)
        {
            $label = "DÉBUT D'EXPOLAROID 2017";
            $expolaroid = mktime(0, 0, 0, 4, 1, 2017);
        }

         $tps_restant = $expolaroid - time();

        //============ CONVERSIONS

        $i_restantes = $tps_restant / 60;
        $H_restantes = $i_restantes / 60;
        $d_restants = $H_restantes / 24;


        $s_restantes = floor($tps_restant % 60); // Secondes restantes
        $i_restantes = floor($i_restantes % 60); // Minutes restantes
        $H_restantes = floor($H_restantes % 24); // Heures restantes
        $d_restants = floor($d_restants); // Jours restants
        //==================

        setlocale(LC_ALL, 'fr_FR');

        return array('nb_day' => $d_restants, 'label_rebourg' => $label);
    }

    public function general()
    {
        $data['actual_link'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $data['sublink'] = str_replace('http://expolaroid.com/', '', str_replace('http://www.expolaroid.com/', '', str_replace('http://expolaroid.guillaumeteillet.com/', '', str_replace('http://www.expolaroid.local/', '', $data['actual_link']))));
        $data['rebourg_array'] = $this->compteARebourg();

        $data['firstname'] = '';
        $data['lastname'] = '';

        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            $data['firstname'] = $this->get('security.token_storage')->getToken()->getUser()->getFirstName();
            $data['lastname'] = $this->get('security.token_storage')->getToken()->getUser()->getLastName();
        }

        $repo = $this->getDoctrine()
            ->getManager()
            ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

        $qb = $repo->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        $qb->where('a.validation_staff = 1');

        $count = $qb->getQuery()->getSingleScalarResult();

        $data['nb_event'] = $count;

        $qb = $repo->createQueryBuilder('a');
        $qb->select('COUNT(DISTINCT a.idUser)');
        $qb->where('a.validation_staff = 1');

        $count_user = $qb->getQuery()->getSingleScalarResult();

        $data['nb_user'] = $count_user;


        $qb = $repo->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        $qb->where('a.validation_staff = 1');
        $qb->andWhere('a.idUser = '.$this->get('security.token_storage')->getToken()->getUser()->getId());

        $count_perso = $qb->getQuery()->getSingleScalarResult();

        $data['nb_event_perso'] = $count_perso;

        return $data;
    }

    public function indexAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $data = $this->general();

             $staff = $this->autorized();

            $repo = $this->getDoctrine()
                ->getManager()
                ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

            $event = $repo->findBy(
                array('idUser' => $this->get('security.token_storage')->getToken()->getUser()->getId())
            );

            $i = 0;

            $ok = 0;
            $ko = 0;
            $wait = 0;
            if (isset($event[$i]))
            {
                while(isset($event[$i]))
                {
                    if ($event[$i]->getValidation_staff() == 1)
                        $ok++;
                    $i++;
                }

                $i = 0;

                while(isset($event[$i]))
                {
                    if ($event[$i]->getValidation_staff() == 0)
                        $wait++;
                    $i++;
                }

                $i = 0;

                while(isset($event[$i]))
                {
                    if ($event[$i]->getValidation_staff() == -1)
                        $ko++;
                    $i++;
                }
            }

            return $this->render('ExpolaroidAdminBundle:Default:index.html.twig', array('staff'=> $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' => $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' =>  $data['firstname'], 'user_lastname' => $data['lastname'], 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso'], 'events_all' => $event, 'nb_ok' => $ok, 'nb_wait' => $wait, 'nb_ko' => $ko));
        }
        return $this->redirect($this->generateUrl('fos_user_security_login'));
    }


    public function creationAction()
    {
        $data = $this->general();
        $staff = $this->autorized();
        return $this->render('ExpolaroidAdminBundle:Default:creation.html.twig', array('staff'=> $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' =>  $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' =>  $data['firstname'], 'user_lastname' => $data['lastname'], 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso']));
    }

    public function createAction()
    {
        $data = $this->general();
        $ok = 0;

        if($_POST['type'] != '' && $_POST['name'] != '' && $_POST['lieu'] != '' && $_POST['day_start'] != '' && $_POST['month_start'] != ''
        && $_POST['day_end'] != '' && $_POST['month_end'] != '' && $_POST['description'] != '')
        {

            $event = new EventExpolaroid();
            $event->setIdUser($this->get('security.token_storage')->getToken()->getUser()->getId());
            $event->setType($_POST['type']);
            $event->setName($_POST['name']);
            $event->setAddress($_POST['lieu']);
            $event->setTown($_POST['ville']);
            $event->setStartDay($_POST['day_start']);
            $event->setStartMonth($_POST['month_start']);
            $event->setEndDay($_POST['day_end']);
            $event->setEndMonth($_POST['month_end']);
            if (!isset($_POST['novernissage']))
            {
                $event->setVernissageDay($_POST['day_vernissage']);
                $event->setVernissageMonth($_POST['month_vernissage']);
            }
            else
            {
                $event->setNoVernissage($_POST['novernissage']);
            }
            $event->setDescription($_POST['description']);
            $event->setParticipants($_POST['participants']);
            $event->setSiteWeb($_POST['siteweb']);
            $event->setContact($_POST['email']);
            $event->setValidation_staff(0);

            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();

            if ($_FILES['fichier']['error'] != 4)
            {
                $content_dir = $this->container->getParameter('kernel.root_dir').'/../web/upload/'; // dossier où sera déplacé le fichier

                $tmp_file = $_FILES['fichier']['tmp_name'];

                if( !is_uploaded_file($tmp_file) )
                {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($event);
                    $em->flush();
                    exit("Le fichier est introuvable");
                }

                // on vérifie maintenant l'extension
                $type_file = $_FILES['fichier']['type'];

                if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($event);
                    $em->flush();
                    exit("Le fichier n'est pas une image");
                }

                if($_FILES['fichier']['size'] > (2048000)) //can't be larger than 2 MB
                {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($event);
                    $em->flush();
                    exit("Le fichier est trop gros");
                }

                // on copie le fichier dans le dossier de destination
                $name_file_ici = $event->getId().".".str_replace('image/', '', $type_file);

                if( !move_uploaded_file($tmp_file, $content_dir . $name_file_ici) )
                {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($event);
                    $em->flush();
                    exit("Impossible de copier le fichier dans $content_dir");
                }

                if ($_FILES['fichier_miniature']['error'] != 4)
                {
                    $content_dir = $this->container->getParameter('kernel.root_dir').'/../web/upload/'; // dossier où sera déplacé le fichier

                    $tmp_file = $_FILES['fichier_miniature']['tmp_name'];

                    if( !is_uploaded_file($tmp_file) )
                    {
                        $em = $this->getDoctrine()->getManager();
                        $em->remove($event);
                        $em->flush();
                        exit("Le fichier est introuvable");
                    }

                    // on vérifie maintenant l'extension
                    $type_file = $_FILES['fichier_miniature']['type'];

                    if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                    {
                        $em = $this->getDoctrine()->getManager();
                        $em->remove($event);
                        $em->flush();
                        exit("Le fichier n'est pas une image");
                    }

                    if($_FILES['fichier_miniature']['size'] > (1024000)) //can't be larger than 1 MB
                    {
                        $em = $this->getDoctrine()->getManager();
                        $em->remove($event);
                        $em->flush();
                        exit("Le fichier est trop gros");
                    }

                    // on copie le fichier dans le dossier de destination
                    $name_file = 'mini_'.$event->getId().".".str_replace('image/', '', $type_file);

                    if( !move_uploaded_file($tmp_file, $content_dir . $name_file) )
                    {
                        $em = $this->getDoctrine()->getManager();
                        $em->remove($event);
                        $em->flush();
                        exit("Impossible de copier le fichier dans $content_dir");
                    }
                }
                else
                {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($event);
                    $em->flush();
                    exit("La miniature est obligatoire");
                }

                $event->setUrlPhoto($name_file_ici);
                $em = $this->getDoctrine()->getManager();
                $em->persist($event);
                $em->flush();

            }
            $ok = 1;
        }

        $staff = $this->autorized();
        return $this->render('ExpolaroidAdminBundle:Default:create.html.twig', array('staff'=> $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' =>  $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' =>  $data['firstname'], 'user_lastname' => $data['lastname'], 'ok' => $ok, 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso']));
    }

    public function adhesionAction()
    {
        $data = $this->general();
        $staff = $this->autorized();
        return $this->render('ExpolaroidAdminBundle:Default:adhesion.html.twig', array('staff'=> $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' => $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' =>  $data['firstname'], 'user_lastname' => $data['lastname'], 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso']));
    }

    public function packgraphiqueAction()
    {
        $data = $this->general();
        $staff = $this->autorized();
        return $this->render('ExpolaroidAdminBundle:Default:packgraphique.html.twig', array('staff'=> $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' => $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' =>  $data['firstname'], 'user_lastname' => $data['lastname'], 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso']));
    }

    public function deleteAction($id)
    {
        $repo = $this->getDoctrine()
            ->getManager()
            ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

        $event = $repo->findBy(
            array('id' =>$id, 'idUser' => $this->get('security.token_storage')->getToken()->getUser()->getId())
        );

        $em = $this->getDoctrine()->getManager();
        $em->remove($event[0]);
        $em->flush();

        return $this->redirect($this->generateUrl('expolaroid_admin_homepage'));
    }

    public function modificationAction($id)
    {
        $staff = $this->autorized();
        $data = $this->general();

        $repo = $this->getDoctrine()
            ->getManager()
            ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

        $event = $repo->findBy(
            array('id' =>$id, 'idUser' => $this->get('security.token_storage')->getToken()->getUser()->getId())
        );

        return $this->render('ExpolaroidAdminBundle:Default:modification.html.twig', array('staff'=> $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' =>  $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' =>  $data['firstname'], 'user_lastname' => $data['lastname'], 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso'], 'event' => $event[0]));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modifAction($id)
    {
        $staff = $this->autorized();
        $data = $this->general();
        $ok = 0;

        if($_POST['type'] != '' && $_POST['name'] != '' && $_POST['lieu'] != '' && $_POST['day_start'] != '' && $_POST['month_start'] != ''
            && $_POST['day_end'] != '' && $_POST['month_end'] != '' && $_POST['description'] != '')
        {

            $repo = $this->getDoctrine()
                ->getManager()
                ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

            $event = $repo->findBy(
                array('id' =>$id, 'idUser' => $this->get('security.token_storage')->getToken()->getUser()->getId())
            );
            $event[0]->setIdUser($this->get('security.token_storage')->getToken()->getUser()->getId());
            $event[0]->setType($_POST['type']);
            $event[0]->setName($_POST['name']);
            $event[0]->setAddress($_POST['lieu']);
            $event[0]->setTown($_POST['ville']);
            $event[0]->setStartDay($_POST['day_start']);
            $event[0]->setStartMonth($_POST['month_start']);
            $event[0]->setEndDay($_POST['day_end']);
            $event[0]->setEndMonth($_POST['month_end']);
            if (!isset($_POST['novernissage']))
            {
                $event[0]->setVernissageDay($_POST['day_vernissage']);
                $event[0]->setVernissageMonth($_POST['month_vernissage']);
            }
            else
            {
                $event[0]->setNoVernissage($_POST['novernissage']);
            }
            $event[0]->setDescription($_POST['description']);
            $event[0]->setParticipants($_POST['participants']);
            $event[0]->setSiteWeb($_POST['siteweb']);
            $event[0]->setContact($_POST['email']);
            $event[0]->setValidation_staff(0);

            $em = $this->getDoctrine()->getManager();
            $em->persist($event[0]);
            $em->flush();


            if ($event[0]->getUrlPhoto() == '')
            {
                if ($_FILES['fichier']['error'] != 4)
                {
                    $content_dir = $this->container->getParameter('kernel.root_dir').'/../web/upload/'; // dossier où sera déplacé le fichier

                    $tmp_file = $_FILES['fichier']['tmp_name'];

                    if( !is_uploaded_file($tmp_file) )
                    {
                        exit("Le fichier est introuvable");
                    }

                    // on vérifie maintenant l'extension
                    $type_file = $_FILES['fichier']['type'];

                    if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                    {
                        exit("Le fichier n'est pas une image");
                    }

                    if($_FILES['fichier']['size'] > (2048000)) //can't be larger than 2 MB
                    {
                        exit("Le fichier est trop gros");
                    }

                    // on copie le fichier dans le dossier de destination
                    $name_file_ici = $event[0]->getId().".".str_replace('image/', '', $type_file);

                    if( !move_uploaded_file($tmp_file, $content_dir . $name_file_ici) )
                    {
                        exit("Impossible de copier le fichier dans $content_dir");
                    }

                    if ($_FILES['fichier_miniature']['error'] != 4)
                    {
                        $content_dir = $this->container->getParameter('kernel.root_dir').'/../web/upload/'; // dossier où sera déplacé le fichier

                        $tmp_file = $_FILES['fichier_miniature']['tmp_name'];

                        if( !is_uploaded_file($tmp_file) )
                        {
                            exit("Le fichier est introuvable");
                        }

                        // on vérifie maintenant l'extension
                        $type_file = $_FILES['fichier_miniature']['type'];

                        if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                        {
                            exit("Le fichier n'est pas une image");
                        }

                        if($_FILES['fichier_miniature']['size'] > (1024000)) //can't be larger than 1 MB
                        {
                            exit("Le fichier est trop gros");
                        }

                        // on copie le fichier dans le dossier de destination
                        $name_file = "mini_".$event[0]->getId().".".str_replace('image/', '', $type_file);

                        if( !move_uploaded_file($tmp_file, $content_dir . $name_file) )
                        {
                            exit("Impossible de copier le fichier dans $content_dir");
                        }
                    }
                    else
                    {
                        exit("Miniature obligatoire");
                    }

                    $event[0]->setUrlMini($name_file);
                    $event[0]->setUrlPhoto($name_file_ici);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($event[0]);
                    $em->flush();
                }
                elseif ($event[0]->getUrlMini() == '' && $event[0]->getUrlPhoto() == '')
                {

                }
                else
                {
                    exit("Miniature obligatoire");
                }
            }
            else
            {
                if ($_FILES['fichier']['error'] != 4)
                {
                    if ($event[0]->getUrlPhoto() != '')
                    {
                        @unlink($this->container->getParameter('kernel.root_dir').'/../web/upload/'.$event[0]->getUrlPhoto());
                    }
                    $content_dir = $this->container->getParameter('kernel.root_dir').'/../web/upload/'; // dossier où sera déplacé le fichier

                    $tmp_file = $_FILES['fichier']['tmp_name'];

                    if( !is_uploaded_file($tmp_file) )
                    {
                        exit("Le fichier est introuvable");
                    }

                    // on vérifie maintenant l'extension
                    $type_file = $_FILES['fichier']['type'];

                    if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                    {
                        exit("Le fichier n'est pas une image");
                    }

                    if($_FILES['fichier']['size'] > (2048000)) //can't be larger than 2 MB
                    {
                        exit("Le fichier est trop gros");
                    }

                    // on copie le fichier dans le dossier de destination
                    $name_file_ici = $event[0]->getId().".".str_replace('image/', '', $type_file);

                    if( !move_uploaded_file($tmp_file, $content_dir . $name_file_ici) )
                    {
                        exit("Impossible de copier le fichier dans $content_dir");
                    }

                    $event[0]->setUrlPhoto($name_file_ici);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($event[0]);
                    $em->flush();
                }

                if ($_FILES['fichier_miniature']['error'] != 4)
                {
                    if ($event[0]->getUrlPhoto() != '')
                    {
                        @unlink($this->container->getParameter('kernel.root_dir').'/../web/upload/mini_'.$event[0]->getUrlPhoto());
                    }

                    $content_dir = $this->container->getParameter('kernel.root_dir').'/../web/upload/'; // dossier où sera déplacé le fichier

                    $tmp_file = $_FILES['fichier_miniature']['tmp_name'];

                    if( !is_uploaded_file($tmp_file) )
                    {
                        exit("Le fichier est introuvable");
                    }

                    // on vérifie maintenant l'extension
                    $type_file = $_FILES['fichier_miniature']['type'];

                    if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                    {
                        exit("Le fichier n'est pas une image");
                    }

                    if($_FILES['fichier_miniature']['size'] > (1024000)) //can't be larger than 1 MB
                    {
                        exit("Le fichier est trop gros");
                    }

                    // on copie le fichier dans le dossier de destination
                    $name_file = "mini_".$event[0]->getId().".".str_replace('image/', '', $type_file);

                    if( !move_uploaded_file($tmp_file, $content_dir . $name_file) )
                    {
                        exit("Impossible de copier le fichier dans $content_dir");
                    }

                    $event[0]->setUrlMini($name_file);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($event[0]);
                    $em->flush();
                }
            }
            $ok = 1;
        }
        return $this->render('ExpolaroidAdminBundle:Default:modif.html.twig', array('staff'=> $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' =>  $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' =>  $data['firstname'], 'user_lastname' => $data['lastname'], 'ok' => $ok, 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso']));
    }

    public function deletepictureAction($id)
    {
        $repo = $this->getDoctrine()
            ->getManager()
            ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

        $event = $repo->findBy(
            array('id' =>$id, 'idUser' => $this->get('security.token_storage')->getToken()->getUser()->getId())
        );
        @unlink($this->container->getParameter('kernel.root_dir').'/../web/upload/'.$event[0]->getUrlPhoto());
        @unlink($this->container->getParameter('kernel.root_dir').'/../web/upload/'.$event[0]->getUrlMini());
        $em = $this->getDoctrine()->getManager();
        $event[0]->setUrlPhoto('');
        $event[0]->setUrlMini('');
        $em->persist($event[0]);
        $em->flush();

        return $this->redirect($this->generateUrl('expolaroid_admin_modification', array('id' => $id)));
    }

    public function exportAction()
    {
        $data = $this->general();
        $staff = $this->autorized();

        if ($staff == 1)
        {
            $repo = $this->getDoctrine()
                ->getManager()
                ->getRepository('UserBundle:User');

            $event = $repo->findAll();
            $i = 0;
            while(isset($event[$i]))
            {
                $list[$i]['name'] = $event[$i]->getLastname();
                $list[$i]['firstname'] = $event[$i]->getFirstname();
                $list[$i]['email'] = $event[$i]->getEmail();
                $list[$i]['city'] = $event[$i]->getTown();
                $list[$i]['phone'] = $event[$i]->getPhone();
                $i++;
            }

            return $this->render('ExpolaroidAdminBundle:Default:export.html.twig', array('contact_all' => $list, 'staff'=> $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' => $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' =>  $data['firstname'], 'user_lastname' => $data['lastname'], 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso']));
        }
        else
        {
            return $this->redirect($this->generateUrl('expolaroid_admin_homepage'));
        }
    }

    public function validationAction()
    {
        $data = $this->general();
        $staff = $this->autorized();

        if ($staff == 1)
        {
            if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
                $data = $this->general();

                $staff = $this->autorized();

                $repo = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

                $event = $repo->findAll();

                $i = 0;

                $repo2 = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('UserBundle:User');

                while(isset($event[$i]))
                {
                    $dataInfos = $repo2->findBy(array('id' => $event[$i]->getIdUser()));

                    $authors[$event[$i]->getId()]['name'] = $dataInfos[0]->getLastName();
                    $authors[$event[$i]->getId()]['firstname'] = $dataInfos[0]->getFirstName();
                    $authors[$event[$i]->getId()]['email'] = $dataInfos[0]->getEmail();
                    $i++;
                }

                $i = 0;

                $ok = 0;
                $ko = 0;
                $wait = 0;
                if (isset($event[$i]))
                {
                    while(isset($event[$i]))
                    {
                        if ($event[$i]->getValidation_staff() == 1)
                            $ok++;
                        $i++;
                    }

                    $i = 0;

                    while(isset($event[$i]))
                    {
                        if ($event[$i]->getValidation_staff() == 0)
                            $wait++;
                        $i++;
                    }

                    $i = 0;

                    while(isset($event[$i]))
                    {
                        if ($event[$i]->getValidation_staff() == -1)
                            $ko++;
                        $i++;
                    }
                }

                return $this->render('ExpolaroidAdminBundle:Default:validation.html.twig', array('authors'=> $authors, 'staff'=> $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' => $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' =>  $data['firstname'], 'user_lastname' => $data['lastname'], 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso'], 'events_all' => $event, 'nb_ok' => $ok, 'nb_wait' => $wait, 'nb_ko' => $ko));
            }
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        else
        {
            return $this->redirect($this->generateUrl('expolaroid_admin_homepage'));
        }
    }

    public function modificationAdminAction($id)
    {
        $staff = $this->autorized();
        $data = $this->general();

        if ($staff == 1)
        {
            $repo = $this->getDoctrine()
                ->getManager()
                ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

            $event = $repo->findBy(
                array('id' =>$id)
            );

            return $this->render('ExpolaroidAdminBundle:Default:modificationAdmin.html.twig', array('staff'=> $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' =>  $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' =>  $data['firstname'], 'user_lastname' => $data['lastname'], 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso'], 'event' => $event[0]));
        }
        else
        {
            return $this->redirect($this->generateUrl('expolaroid_admin_homepage'));
        }
    }


    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modifAdminAction($id)
    {
        $staff = $this->autorized();
        $data = $this->general();
        $ok = 0;

        if ($staff == 1)
        {
            if($_POST['type'] != '' && $_POST['name'] != '' && $_POST['lieu'] != '' && $_POST['day_start'] != '' && $_POST['month_start'] != ''
                && $_POST['day_end'] != '' && $_POST['month_end'] != '' && $_POST['description'] != '')
            {

                $repo = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

                $event = $repo->findBy(
                    array('id' =>$id)
                );

                $event[0]->setType($_POST['type']);
                $event[0]->setName($_POST['name']);
                $event[0]->setAddress($_POST['lieu']);
                $event[0]->setTown($_POST['ville']);
                $event[0]->setStartDay($_POST['day_start']);
                $event[0]->setStartMonth($_POST['month_start']);
                $event[0]->setEndDay($_POST['day_end']);
                $event[0]->setEndMonth($_POST['month_end']);
                if (!isset($_POST['novernissage']))
                {
                    $event[0]->setVernissageDay($_POST['day_vernissage']);
                    $event[0]->setVernissageMonth($_POST['month_vernissage']);
                }
                else
                {
                    $event[0]->setNoVernissage($_POST['novernissage']);
                }
                $event[0]->setDescription($_POST['description']);
                $event[0]->setParticipants($_POST['participants']);
                $event[0]->setSiteWeb($_POST['siteweb']);
                $event[0]->setContact($_POST['email']);

                $em = $this->getDoctrine()->getManager();
                $em->persist($event[0]);
                $em->flush();


                if ($event[0]->getUrlPhoto() == '')
                {
                    if ($_FILES['fichier']['error'] != 4)
                    {
                        $content_dir = $this->container->getParameter('kernel.root_dir').'/../web/upload/'; // dossier où sera déplacé le fichier

                        $tmp_file = $_FILES['fichier']['tmp_name'];

                        if( !is_uploaded_file($tmp_file) )
                        {
                            exit("Le fichier est introuvable");
                        }

                        // on vérifie maintenant l'extension
                        $type_file = $_FILES['fichier']['type'];

                        if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                        {
                            exit("Le fichier n'est pas une image");
                        }

                        if($_FILES['fichier']['size'] > (2048000)) //can't be larger than 2 MB
                        {
                            exit("Le fichier est trop gros");
                        }

                        // on copie le fichier dans le dossier de destination
                        $name_file_ici = $event[0]->getId().".".str_replace('image/', '', $type_file);

                        if( !move_uploaded_file($tmp_file, $content_dir . $name_file_ici) )
                        {
                            exit("Impossible de copier le fichier dans $content_dir");
                        }

                        if ($_FILES['fichier_miniature']['error'] != 4)
                        {
                            $content_dir = $this->container->getParameter('kernel.root_dir').'/../web/upload/'; // dossier où sera déplacé le fichier

                            $tmp_file = $_FILES['fichier_miniature']['tmp_name'];

                            if( !is_uploaded_file($tmp_file) )
                            {
                                exit("Le fichier est introuvable");
                            }

                            // on vérifie maintenant l'extension
                            $type_file = $_FILES['fichier_miniature']['type'];

                            if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                            {
                                exit("Le fichier n'est pas une image");
                            }

                            if($_FILES['fichier_miniature']['size'] > (1024000)) //can't be larger than 1 MB
                            {
                                exit("Le fichier est trop gros");
                            }

                            // on copie le fichier dans le dossier de destination
                            $name_file = "mini_".$event[0]->getId().".".str_replace('image/', '', $type_file);

                            if( !move_uploaded_file($tmp_file, $content_dir . $name_file) )
                            {
                                exit("Impossible de copier le fichier dans $content_dir");
                            }
                        }
                        else
                        {
                            exit("Miniature obligatoire");
                        }

                        $event[0]->setUrlMini($name_file);
                        $event[0]->setUrlPhoto($name_file_ici);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($event[0]);
                        $em->flush();
                    }
                    elseif ($event[0]->getUrlMini() == '' && $event[0]->getUrlPhoto() == '')
                    {

                    }
                    else
                    {
                        exit("Miniature obligatoire");
                    }
                }
                else
                {
                    if ($_FILES['fichier']['error'] != 4)
                    {
                        if ($event[0]->getUrlPhoto() != '')
                        {
                            @unlink($this->container->getParameter('kernel.root_dir').'/../web/upload/'.$event[0]->getUrlPhoto());
                        }
                        $content_dir = $this->container->getParameter('kernel.root_dir').'/../web/upload/'; // dossier où sera déplacé le fichier

                        $tmp_file = $_FILES['fichier']['tmp_name'];

                        if( !is_uploaded_file($tmp_file) )
                        {
                            exit("Le fichier est introuvable");
                        }

                        // on vérifie maintenant l'extension
                        $type_file = $_FILES['fichier']['type'];

                        if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                        {
                            exit("Le fichier n'est pas une image");
                        }

                        if($_FILES['fichier']['size'] > (2048000)) //can't be larger than 2 MB
                        {
                            exit("Le fichier est trop gros");
                        }

                        // on copie le fichier dans le dossier de destination
                        $name_file_ici = $event[0]->getId().".".str_replace('image/', '', $type_file);

                        if( !move_uploaded_file($tmp_file, $content_dir . $name_file_ici) )
                        {
                            exit("Impossible de copier le fichier dans $content_dir");
                        }

                        $event[0]->setUrlPhoto($name_file_ici);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($event[0]);
                        $em->flush();
                    }

                    if ($_FILES['fichier_miniature']['error'] != 4)
                    {
                        if ($event[0]->getUrlPhoto() != '')
                        {
                            @unlink($this->container->getParameter('kernel.root_dir').'/../web/upload/mini_'.$event[0]->getUrlPhoto());
                        }

                        $content_dir = $this->container->getParameter('kernel.root_dir').'/../web/upload/'; // dossier où sera déplacé le fichier

                        $tmp_file = $_FILES['fichier_miniature']['tmp_name'];

                        if( !is_uploaded_file($tmp_file) )
                        {
                            exit("Le fichier est introuvable");
                        }

                        // on vérifie maintenant l'extension
                        $type_file = $_FILES['fichier_miniature']['type'];

                        if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                        {
                            exit("Le fichier n'est pas une image");
                        }

                        if($_FILES['fichier_miniature']['size'] > (1024000)) //can't be larger than 1 MB
                        {
                            exit("Le fichier est trop gros");
                        }

                        // on copie le fichier dans le dossier de destination
                        $name_file = "mini_".$event[0]->getId().".".str_replace('image/', '', $type_file);

                        if( !move_uploaded_file($tmp_file, $content_dir . $name_file) )
                        {
                            exit("Impossible de copier le fichier dans $content_dir");
                        }

                        $event[0]->setUrlMini($name_file);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($event[0]);
                        $em->flush();
                    }
                }
                $ok = 1;
            }
            return $this->render('ExpolaroidAdminBundle:Default:modifAdmin.html.twig', array('staff'=> $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' =>  $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' =>  $data['firstname'], 'user_lastname' => $data['lastname'], 'ok' => $ok, 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso']));
        }
        else
        {
            return $this->redirect($this->generateUrl('expolaroid_admin_homepage'));
        }
    }

    public function approveAction($id)
    {
        $staff = $this->autorized();
        $data = $this->general();

        if ($staff == 1)
        {
            $repo = $this->getDoctrine()
                ->getManager()
                ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

            $event = $repo->findBy(
                array('id' =>$id)
            );

            $event[0]->setValidation_staff(1);

            $em = $this->getDoctrine()->getManager();
            $em->persist($event[0]);
            $em->flush();

            $repoUser = $this->getDoctrine()
                ->getManager()
                ->getRepository('UserBundle:User');

            $user = $repoUser->findOneBy(
                array('id' => $event[0]->getIdUser())
            );

            $message = \Swift_Message::newInstance()
                ->setSubject('Votre événement Expolaroid a été accepté.')
                ->setFrom('noreply@expolaroid.com', 'Charlotte')
                ->setTo($user->getEmail())
                ->setBody("Bonjour,<br/><br/> Votre événement Expolaroid a été accepté<br/><br/>Cordialement,<br/><br/>La team Expolaroid")
                ->setContentType('text/html');
            ;
            $this->get('mailer')->send($message);

            return $this->render('ExpolaroidAdminBundle:Default:approveAdmin.html.twig', array('ok' => 1, 'staff'=> $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' =>  $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' =>  $data['firstname'], 'user_lastname' => $data['lastname'], 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso'], 'event' => $event[0]));
        }
        else
        {
            return $this->redirect($this->generateUrl('expolaroid_admin_homepage'));
        }
    }
    public function refuseAction($id)
    {
        $staff = $this->autorized();
        $data = $this->general();

        if ($staff == 1)
        {
            $repo = $this->getDoctrine()
                ->getManager()
                ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

            $event = $repo->findBy(
                array('id' =>$id)
            );

            return $this->render('ExpolaroidAdminBundle:Default:refuse.html.twig', array('staff'=> $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' =>  $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' =>  $data['firstname'], 'user_lastname' => $data['lastname'], 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso'], 'event' => $event[0]));
        }
        else
        {
            return $this->redirect($this->generateUrl('expolaroid_admin_homepage'));
        }
    }

    public function refuseAdminAction($id)
    {
        $staff = $this->autorized();
        $data = $this->general();

        if ($staff == 1)
        {
            $repo = $this->getDoctrine()
                ->getManager()
                ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

            $event = $repo->findBy(
                array('id' =>$id)
            );

            $event[0]->setValidation_staff(-1);
            $event[0]->setMotif($_POST['motif']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($event[0]);
            $em->flush();

            $repoUser = $this->getDoctrine()
                ->getManager()
                ->getRepository('UserBundle:User');

            $user = $repoUser->findOneBy(
                array('id' => $event[0]->getIdUser())
            );

            $message = \Swift_Message::newInstance()
                ->setSubject('Votre événement Expolaroid a été refusé.')
                ->setFrom('noreply@expolaroid.com', 'Charlotte')
                ->setTo($user->getEmail())
                ->setBody("Bonjour,<br/><br/> Votre événement Expolaroid a été refusé. Motif : ".$_POST['motif']."<br/><br/>Cordialement,<br/><br/>La team Expolaroid")
                ->setContentType('text/html');
            ;
            $this->get('mailer')->send($message);

            return $this->render('ExpolaroidAdminBundle:Default:refuseAdmin.html.twig', array('ok' => 1,'staff'=> $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' =>  $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' =>  $data['firstname'], 'user_lastname' => $data['lastname'], 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso'], 'event' => $event[0]));
        }
        else
        {
            return $this->redirect($this->generateUrl('expolaroid_admin_homepage'));
        }
    }

    public function managePartnersAction()
    {
        $staff = $this->autorized();
        $data = $this->general();
        $updateStatus = 'none';
        $entityManager = $this->getDoctrine()->getManager();
        $expoRepository = $entityManager->getRepository('ExpolaroidAdminBundle:Partners');
        $activePartners = $expoRepository->find(1);

        if ($staff == 1) {
            if (isset($_POST["modifImg"])) {
                if (isset($_FILES['img'])) {
                    $dossier = 'upload/partners/';
                    $fichier = basename($_FILES['img']['name']);
                    if (move_uploaded_file($_FILES['img']['tmp_name'], $dossier . $fichier)) {
                        $updateStatus = 'true';
                    } else {
                        $updateStatus = 'false';
                    }
                }
            }
            if (isset($_POST["modifPartners"]))
            {
                $activePartners->setContent($_POST['partners']);
                $entityManager->persist($activePartners);
                $entityManager->flush();
            }
            return $this->render('ExpolaroidAdminBundle:Default:managePartners.html.twig', array('staff' => $staff, 'label_rebourg' => $data['rebourg_array']['label_rebourg'], 'compteARebourg' => $data['rebourg_array']['nb_day'], 'sublink' => $data['sublink'], 'user_firstname' => $data['firstname'], 'user_lastname' => $data['lastname'], 'nb_event' => $data['nb_event'], 'nb_users' => $data['nb_user'], 'nb_event_perso' => $data['nb_event_perso'], 'updateStatus' => $updateStatus, 'filesName' => $this->getFilesName(), 'activePartners' => $activePartners->getContent()));
        }
        else
        {
            return $this->redirect($this->generateUrl('expolaroid_admin_homepage'));
        }
    }

    private function getFilesName(){
        $files = scandir('upload/partners/', SCANDIR_SORT_ASCENDING);
        unset($files[0], $files[1]); // remove . && .. from $files
        return $files;
    }

}
