<?php

namespace Expolaroid\ShowcaseBundle\Controller;

use Expolaroid\AdminBundle\Entity\EventExpolaroid;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    private function loadPartners(){
        $entityManager = $this->getDoctrine()->getManager();
        $expoRepository = $entityManager->getRepository('ExpolaroidAdminBundle:Partners');
        $activePartners = $expoRepository->find(1);
        return $activePartners->getContent();
    }

    public function indexAction()
    {

        $repo = $this->getDoctrine()
            ->getManager()
            ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

        $result = $repo->findBy(array('validation_staff' => 1));

        $i = 0;

        $data = array();
        if (isset($result))
        {
            while($i < sizeof($result))
            {
                $Address = urlencode($result[$i]->getAddress());
                $request_url = "https://maps.googleapis.com/maps/api/geocode/xml?address=".$Address."&sensor=true&key=AIzaSyCTTMIzjS6GTlHjekeLFVYz_XaPT9LrS5s";
                $xml = simplexml_load_file($request_url);
                $status = $xml->status;
                if ($status=="OK") {
                    $Lat = $xml->result->geometry->location->lat;
                    $Lon = $xml->result->geometry->location->lng;
                    $data[$i]['lat'] = $Lat;
                    $data[$i]['long'] = $Lon;
                    $data[$i]['i'] = $i;
                    $data[$i]['name'] = $result[$i]->getName();
                    $data[$i]['address'] = $result[$i]->getAddress();
                    $data[$i]['dayStart'] = $result[$i]->getStartDay();
                    $data[$i]['monthStart'] = $result[$i]->getStartMonth();
                    $data[$i]['dayEnd'] = $result[$i]->getEndDay();
                    $data[$i]['monthEnd'] = $result[$i]->getEndMonth();
                    $data[$i]['noVernissage'] = $result[$i]->getNoVernissage();
                    $data[$i]['dayVernissage'] = $result[$i]->getVernissageDay();
                    $data[$i]['monthVernissage'] = $result[$i]->getVernissageMonth();
                    $data[$i]['description'] = $result[$i]->getDescription();
                    $data[$i]['siteWeb'] = $result[$i]->getSiteWeb();
                    $data[$i]['contact'] = $result[$i]->getContact();
                }
                $i++;
            }
        }

        return $this->render('ExpolaroidShowcaseBundle:Default:index.html.twig', array('data' => $data, 'partnersList' => $this->loadPartners()));
    }

    public function aboutAction()
    {
        return $this->render('ExpolaroidShowcaseBundle:Default:about.html.twig', array('partnersList' => $this->loadPartners()));
    }

    public function connexionAction()
    {
        return $this->render('ExpolaroidShowcaseBundle:Default:connexion.html.twig', array('partnersList' => $this->loadPartners()));
    }

    public function eventsAction($page = 1)
    {
        $repo = $this->getDoctrine()
            ->getManager()
            ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

        $resultat_par_page = 3;

        if ($page == 1)
            $start = 0;
        else
            $start = ($page - 1) * $resultat_par_page;

        if (isset($_GET['filter']) && $_GET['filter'] == "expo")
            $filtre = 'expo';
        elseif (isset($_GET['filter']) && $_GET['filter'] == "concours")
            $filtre = 'concours';
        elseif (isset($_GET['filter']) && $_GET['filter'] == "atelier")
            $filtre = 'ateliers';
        elseif (isset($_GET['filter']) && $_GET['filter'] == "autre")
            $filtre = 'autres';
        else
            $filtre = 'all';

        if ($filtre != 'all')
            $result = $repo->findBy(array('validation_staff' => 1, 'type' => $filtre), null, $resultat_par_page, $start);
        else
            $result = $repo->findBy(array('validation_staff' => 1), null, $resultat_par_page, $start);

        $repo = $this->getDoctrine()
            ->getManager()
            ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

        $qb = $repo->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        $qb->where('a.validation_staff = 1');
        if ($filtre != 'all')
            $qb->andWhere("a.type = '".$filtre."'");

        $count = $qb->getQuery()->getSingleScalarResult();
        $nb_page = ceil($count / $resultat_par_page);

        $i = 0;
        foreach ($result as $data)
        {
            $repoUser = $this->getDoctrine()
                ->getManager()
                ->getRepository('UserBundle:User');

            $user = $repoUser->findOneBy(
                array('id' => $data->getIdUser())
            );

            if ($user != null)
                $resultCity[$i] = $user->getTown();
            else
                $resultCity[$i] = "";
            $i++;
        }


        return $this->render('ExpolaroidShowcaseBundle:Default:events.html.twig', array('resultCity' => $resultCity, 'data' => $result, 'filter' => $filtre, 'nb_page_max' => $nb_page, 'page_actuelle' => $page, 'partnersList' => $this->loadPartners()));
    }


    public function eventsResultsAction($page = 1)
    {

        $recherche = strip_tags($_POST['recherche']);

        $repo = $this->getDoctrine()
            ->getManager()
            ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

        $resultat_par_page = 3;

        if ($page == 1)
            $start = 0;
        else
            $start = ($page - 1) * $resultat_par_page;

        if (isset($_GET['filter']) && $_GET['filter'] == "expo")
            $filtre = 'expo';
        elseif (isset($_GET['filter']) && $_GET['filter'] == "concours")
            $filtre = 'concours';
        elseif (isset($_GET['filter']) && $_GET['filter'] == "atelier")
            $filtre = 'ateliers';
        elseif (isset($_GET['filter']) && $_GET['filter'] == "autre")
            $filtre = 'autres';
        else
            $filtre = 'all';

        if ($filtre != 'all')
        {
            $qb = $repo->createQueryBuilder('a');
            $qb->where('a.validation_staff = 1');
            $qb->andWhere('a.name LIKE :query');
            $qb->andWhere('a.type = :type');
            $qb->orWhere('a.address LIKE :query');
            $qb->orWhere('a.description LIKE :query');
            $qb->orWhere('a.participants LIKE :query');
            $qb->orWhere('a.siteWeb LIKE :query');
            $qb->setParameter('query', '%'. $recherche.'%');
            $qb->setParameter('type',  $filtre);
            $qb->setFirstResult($start);
            $qb->setMaxResults($resultat_par_page);// $resultat_par_page, $start

            $result = $qb->getQuery()->getScalarResult();
        }
        else
        {
            $qb = $repo->createQueryBuilder('a');
            $qb->where('a.validation_staff = 1');
            $qb->andWhere('a.name LIKE :query');
            $qb->orWhere('a.address LIKE :query');
            $qb->orWhere('a.description LIKE :query');
            $qb->orWhere('a.participants LIKE :query');
            $qb->orWhere('a.siteWeb LIKE :query');
            $qb->setParameter('query', '%'. $recherche.'%');
            $qb->setFirstResult($start);
            $qb->setMaxResults($resultat_par_page);

            $result = $qb->getQuery()->getScalarResult();
        }

        $repo = $this->getDoctrine()
            ->getManager()
            ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');

        $qb = $repo->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        $qb->where('a.validation_staff = 1');
        $qb->andWhere('a.name LIKE :query');
        $qb->orWhere('a.address LIKE :query');
        $qb->orWhere('a.description LIKE :query');
        $qb->orWhere('a.participants LIKE :query');
        $qb->orWhere('a.siteWeb LIKE :query');
        $qb->setParameter('query', '%'. $recherche.'%');
        if ($filtre != 'all')
            $qb->andWhere("a.type = '".$filtre."'");




        $count = $qb->getQuery()->getSingleScalarResult();
        $nb_page = ceil($count / $resultat_par_page);

        $i = 0;
        foreach ($result as $data)
        {
            $repoUser = $this->getDoctrine()
                ->getManager()
                ->getRepository('UserBundle:User');

            $user = $repoUser->findOneBy(
                array('id' => $data['a_idUser'])
            );

            if ($user != null)
                $resultCity[$i] = $user->getTown();
            else
                $resultCity[$i] = "";
            $i++;
        }


        return $this->render('ExpolaroidShowcaseBundle:Default:eventsResults.html.twig', array('resultCity' => $resultCity, 'data' => $result, 'filter' => $filtre, 'nb_page_max' => $nb_page, 'page_actuelle' => $page, 'partnersList' => $this->loadPartners()));
    }

    public function step1Action()
    {
        return $this->render('ExpolaroidShowcaseBundle:Default:step1.html.twig', array('partnersList' => $this->loadPartners()));
    }

    public function step2Action()
    {
        return $this->render('ExpolaroidShowcaseBundle:Default:step2.html.twig', array('partnersList' => $this->loadPartners()));
    }

    public function step4Action()
    {
        return $this->render('ExpolaroidShowcaseBundle:Default:step4.html.twig', array('partnersList' => $this->loadPartners()));
    }

    public function step5Action()
    {
        if($_POST['type'] != '' && $_POST['name'] != '' && $_POST['lieu'] != '' && $_POST['day_start'] != '' && $_POST['month_start'] != ''
            && $_POST['day_end'] != '' && $_POST['month_end'] != '' && $_POST['description'] != '')
        {

            $event = new EventExpolaroid();
            $event->setIdUser($this->get('security.token_storage')->getToken()->getUser()->getId());
            $event->setType($_POST['type']);
            $event->setName($_POST['name']);
            $event->setAddress($_POST['lieu']);
            $event->setTown($_POST['ville']);
            $event->setStartDay($_POST['day_start']);
            $event->setStartMonth($_POST['month_start']);
            $event->setEndDay($_POST['day_end']);
            $event->setEndMonth($_POST['month_end']);
            if (!isset($_POST['novernissage']))
            {
                $event->setVernissageDay($_POST['day_vernissage']);
                $event->setVernissageMonth($_POST['month_vernissage']);
            }
            else
            {
                $event->setNoVernissage($_POST['novernissage']);
            }
            $event->setDescription($_POST['description']);
            $event->setParticipants($_POST['participants']);
            $event->setSiteWeb($_POST['siteweb']);
            $event->setContact($_POST['email']);
            $event->setValidation_staff(0);

            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();

            if ($_FILES['fichier']['error'] != 4)
            {
                $content_dir = $this->container->getParameter('kernel.root_dir').'/../web/upload/'; // dossier où sera déplacé le fichier

                $tmp_file = $_FILES['fichier']['tmp_name'];

                if( !is_uploaded_file($tmp_file) )
                {
                    exit("Le fichier est introuvable");
                }

                // on vérifie maintenant l'extension
                $type_file = $_FILES['fichier']['type'];

                if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                {
                    exit("Le fichier n'est pas une image");
                }

                // on copie le fichier dans le dossier de destination
                $name_file_ici = $event->getId().".".str_replace('image/', '', $type_file);

                if( !move_uploaded_file($tmp_file, $content_dir . $name_file_ici) )
                {
                    exit("Impossible de copier le fichier dans $content_dir");
                }

                if($_FILES['fichier']['size'] > (2048000)) //can't be larger than 1 MB
                {
                    exit("Le fichier est trop gros");
                }

                if ($_FILES['fichier_miniature']['error'] != 4)
                {
                    $content_dir = $this->container->getParameter('kernel.root_dir').'/../web/upload/'; // dossier où sera déplacé le fichier

                    $tmp_file = $_FILES['fichier_miniature']['tmp_name'];

                    if( !is_uploaded_file($tmp_file) )
                    {
                        exit("Le fichier est introuvable");
                    }

                    // on vérifie maintenant l'extension
                    $type_file = $_FILES['fichier_miniature']['type'];

                    if( !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'png') )
                    {
                        exit("Le fichier n'est pas une image");
                    }

                    if($_FILES['fichier_miniature']['size'] > (1024000)) //can't be larger than 1 MB
                    {
                        exit("Le fichier est trop gros");
                    }

                    // on copie le fichier dans le dossier de destination
                    $name_file = "mini_".$event->getId().".".str_replace('image/', '', $type_file);

                    if( !move_uploaded_file($tmp_file, $content_dir . $name_file) )
                    {
                        exit("Impossible de copier le fichier dans $content_dir");
                    }
                }
                else
                {
                    exit("Miniature obligatoire");
                }

                $event->setUrlMini($name_file);
                $event->setUrlPhoto($name_file_ici);
                $em = $this->getDoctrine()->getManager();
                $em->persist($event);
                $em->flush();
            }
        }
        return $this->render('ExpolaroidShowcaseBundle:Default:step5.html.twig', array('partnersList' => $this->loadPartners()));
    }

    public function suggestionsAction()
    {
        $repo = $this->getDoctrine()
            ->getManager()
            ->getRepository('ExpolaroidAdminBundle:EventExpolaroid');
        $query = $_GET['query'];

        if(isset($query)) {
            // Mot tapé par l'utilisateur
            $q = htmlentities($_GET['query']);

            $qb = $repo->createQueryBuilder('a');
            $qb->where('a.validation_staff = 1');
            $qb->andWhere('a.name LIKE :query');
            $qb->orWhere('a.address LIKE :query');
            $qb->orWhere('a.description LIKE :query');
            $qb->orWhere('a.participants LIKE :query');
            $qb->orWhere('a.siteWeb LIKE :query');
            $qb->setParameter('query', '%'. $query.'%');

            $result = $qb->getQuery()->getScalarResult();

         //   print_r($result);
            $suggestions['suggestions'][] = "";

            $i = 0;
            // On parcourt les résultats de la requête SQL
            while(isset($result[$i])) {
                // On ajoute les données dans un tableau
                $suggestions['suggestions'][] = $result[$i]['a_name'];
                $suggestions['suggestions'][] = $result[$i]['a_address'];
                $suggestions['suggestions'][] = $result[$i]['a_description'];
                $suggestions['suggestions'][] = $result[$i]['a_participants'];
                $suggestions['suggestions'][] = $result[$i]['a_siteWeb'];
                $i++;
            }


            // On renvoie le données au format JSON pour le plugin
            echo json_encode($suggestions);
            return $this->render('ExpolaroidShowcaseBundle:Default:suggestions.html.twig', array('partnersList' => $this->loadPartners()));
        }
    }
}
